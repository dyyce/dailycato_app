import * as detailTypes from '../modules/constants/detailViewConstants';

const INIT_DATA = {
  offers: [],
  loading: false
}

const home = (state = INIT_DATA, action) => {
  switch(action.type) {
    case detailTypes.GET_OFFERS_BY_ID_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case detailTypes.GET_OFFERS_BY_ID_SUCCESS:
      return Object.assign({}, state, {
        loading: true,
        offers: action.offers
      });
    case detailTypes.GET_OFFERS_BY_ID_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })

    default:
      return state;
  }
}

export default home;
