import * as homeTypes from '../modules/constants/homeConstants';

const INIT_DATA = {
  position: null,
  locatedRestaurants: [],
  loading: false
}

const home = (state = INIT_DATA, action) => {
  switch(action.type) {
    case homeTypes.LOCATE_USER_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case homeTypes.GET_RESTAURANTS_LOCATION_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case homeTypes.GET_RESTAURANTS_LOCATION_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        locatedRestaurants: action.locatedRestaurants
      })
    case homeTypes.GET_RESTAURANTS_LOCATION_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })

    default:
      return state;
  }
}

export default home;
