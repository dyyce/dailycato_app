import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import detailReducer from './detailReducer';

const reducer = combineReducers({
  homeReducer,
  detailReducer
});

export default reducer;
