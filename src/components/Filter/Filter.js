import React, { Component, PropTypes } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  Text,
  View,
} from 'react-native'
import { Actions } from 'react-native-router-flux'

const iconBtn = (
  <Icon.Button name='facebook' backgroundColor='#3b5998'>
    <Text style={{fontFamily: 'Arial', fontSize: 15}}>Login with Facebook</Text>
  </Icon.Button>
)

class Filter extends Component {
  render() {
    return <View style={{marginTop: 65}}>
      {/* <Text ></Text> */}
      <Icon onPress={() => Actions.pop()} name="close" size={30} color="#232323" />
    </View>
  }
}

export default Filter
