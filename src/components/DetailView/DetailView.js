import React, { Component, PropTypes } from 'react'
import {
  View,
  Text,
  StyleSheet,
  MapView,
  TouchableHighlight,
  TouchableWithoutFeedback,
  LayoutAnimation
} from 'react-native'
import SubNavigator from '../dumb/SubNavigator'
import Swiper from 'react-native-swiper'

// {this.props.passProps.doc.name}

const styles = StyleSheet.create({
  container: {
    marginTop: 64,
    flex: 1,
    flexDirection: 'column'
  },
  mapContainer: {
    backgroundColor: 'yellow'
  },
  normalMap: {
    flex: 4,
  },
  extendedMap: {
    flex: 7
  },

  callToActionBtn: {
    position: 'absolute',
    // flex: 2,
    // height: 40,
    zIndex: 0,
    backgroundColor: '#F48B22',
    borderRadius: 4,
    padding: 8,
  },
  callToActionBtnTxt: {
    color: 'white',
    fontWeight: 'bold'
  },

  titleContainer: {
    flex: 1,
    backgroundColor: '#363636',
    justifyContent: 'center',
    alignItems: 'center'
  },
  restaurantInfo: {
    flex: 8,
    // justifyContent: 'center',
    top: 60,
    alignItems: 'center'
  },
  title: {
    color: '#fff',
  },

  infoHeader: {
    fontWeight: 'bold',
    fontSize: 22
  },

  swipeContainer: {
    flex: 4,
    backgroundColor: 'white',
  },
  swiper: {
    flex: 1
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
})

class DetailView extends Component {
  componentWillMount() {
    console.log(this.props)
    this.state = {
      swiperHeight: 0,
      fullscreenMap: false,
      showRestaurantInfo: false,
      showCallToActionBtn: true
    }
    this.props.getOffers(this.props.passProps.doc.id)
  }

  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut();
  }

  changeCallToActionBtnLocation() {
    return this.state.showCallToActionBtn
      ? { top: 230, right: 5 }
      : { top: 230, right: -100 }
  }

  renderMapView() {
    return !this.state.showRestaurantInfo
      ? <View style={this.state.fullscreenMap
      ? [styles.mapContainer, styles.extendedMap]
      : [styles.mapContainer, styles.normalMap]
    }>
      <MapView
        style={{ flex: 1 }}
        showsUserLocation={true}
        annotations={[{
          latitude: this.props.passProps.doc.location.coordinates[0],
          longitude: this.props.passProps.doc.location.coordinates[1]
        }]}
        region={{
          latitude: this.props.passProps.doc.location.coordinates[0],
          longitude: this.props.passProps.doc.location.coordinates[1],
          latitudeDelta: 0.009,
          longitudeDelta: 0.009
        }}
      />
    </View>
    : <View></View>
  }

  renderCallToActionBtn() {
    return this.state.showCallToActionBtn
      ? <TouchableHighlight
          style={[styles.callToActionBtn, this.changeCallToActionBtnLocation()]}
          underlayColor='#F09132'
          onPress={() => {
            this.setState({
              fullscreenMap: !this.state.fullscreenMap,
              showCallToActionBtn: !this.state.showCallToActionBtn
            })
          }}
        >
          <Text style={styles.callToActionBtnTxt}>Los geht's!</Text>
        </TouchableHighlight>
      : <View></View>
  }

  renderTitleBar() {
    return <TouchableHighlight
      style={styles.titleContainer}
      underlayColor='black' //TODO: change later to another color, something more native
      onPress={() => {
        if(this.state.fullscreenMap) {
          this.setState({
            fullscreenMap: !this.state.fullscreenMap,
            showCallToActionBtn: !this.state.callToActionBtn
          })
        } else {
          this.setState({
            showRestaurantInfo: !this.state.showRestaurantInfo,
            showCallToActionBtn: !this.state.showCallToActionBtn
          })
        }
      }}
    >
      <Text style={styles.title}>{this.props.passProps.doc.name}</Text>
    </TouchableHighlight>;
  }

  renderRestaurantInfo() {
    return this.state.showRestaurantInfo
      ? <View style={styles.restaurantInfo}>
          <Text style={styles.infoHeader}>Adresse</Text>
          <Text>{this.props.passProps.doc.street}</Text>
          <Text style={styles.infoHeader}>Öffnungszeiten</Text>
          <Text>{this.props.passProps.doc.openingTime}</Text>
        </View>
      : <View></View>
  }

  renderSwiper() {
    return this.state.fullscreenMap || this.state.showRestaurantInfo
      ? <View></View>
      : <View style={styles.swipeContainer} onLayout={(event) => {
        let { x, y, height, width } = event.nativeEvent.layout;
        this.setState({
          swiperHeight: height
        })
      }}>
        <Swiper
          height={this.state.swiperHeight}
          showsButtons={true}
          dot={<View style={{backgroundColor: 'rgba(0,0,0,0)'}}></View>}
          activeDot={<View style={{backgroundColor: 'rgba(0,0,0,0)'}}></View>}
          loop={false}>
          {this.renderOffers()}
        </Swiper>
      </View>
  }

  renderOffers() {
    let list = this.props.offers.map((item, key) => {
      return <View key={key} style={styles.slide}>
        <Text>{item.name}</Text>
      </View>
    })
    return list;
  }

  render() {
    return <View style={styles.container}>
      {/* <View>
        <SubNavigator filter={false}/>
      </View> */}
      {this.renderMapView()}
      {this.renderCallToActionBtn()}
      {this.renderTitleBar()}
      {this.renderRestaurantInfo()}
      {this.renderSwiper()}
    </View>
  }
}

export default DetailView;
