import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
// import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import routes from '../../routes';

// var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');
import GooglePlaces from '../dumb/GooglePlaces';

// const homePlace = {description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
// const workPlace = {description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};
let fixtures = [
  {description: 'Berlin', geometry: {location: {lat: 52.5200, lng: 13.4050} }},
  {description: 'Hamburg', geometry: {location: {lat: 53.5511, lng: 9.9937} }},
  {description: 'München', geometry: {location: {lat: 48.1351, lng: 11.5820} }},
  {description: 'Würzburg', geometry: {location: {lat: 49.7913, lng: 9.9534} }},
  // {description: 'Aschaffenburg', geometry: {location: {lat: 49.9807, lng: 9.1356} }}
];

// AIzaSyCcoU0umIB4G1i1BiSOqtyeoM2IqBuWE1g

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 70,
    backgroundColor:'#232323',
    justifyContent: 'flex-start'
  },
  upperContainer: {
    flex: 1,
    paddingTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  middleContainer: {
    flex: 2
  },
  lowerContainer: {
    flex: 1
  },
  header: {
    color: '#FF5A00',
    fontWeight: 'bold',
    fontSize: 32,
  },
  subHeader: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20
  },
  searchBar: {
    backgroundColor: '#232323',
    marginRight: 10,
    marginLeft: 10
  }
}

class Home extends Component {
  componentWillMount() {

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperContainer}>
          <Text style={styles.header}>
            HUNGER?
          </Text>
          <Text style={styles.subHeader}>
            FINDE TOLLE TAGES- {'\n'}
            UND MITTAGSANGEBOTE {'\n'}
            IN DEINER NÄHE!
          </Text>

        </View>
        <View style={styles.middleContainer}>
          <View style={styles.searchBar}>
            <GooglePlaces
              style={styles.searchBar}
              placeholder='Stadt eingeben, z.B. Würzburg'
              minLength={2} // minimum length of text to search
              autoFocus={false}
              fetchDetails={true}
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                console.log('data', data);
                console.log(details);
                this.props.getRestaurantsAtLocation(data.geometry.location.lat, data.geometry.location.lng);
                Actions.resultList({ title: data.description });
              }}
              getDefaultValue={() => {
                return ''; // text input default value
              }}
              enablePoweredByContainer={false}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: 'AIzaSyCcoU0umIB4G1i1BiSOqtyeoM2IqBuWE1g',
                language: 'de', // language of the results
                types: '(cities)', // default: 'geocode'
              }}

              currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
              currentLocationLabel="Aktuelle Position"
              nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              GoogleReverseGeocodingQuery={{
                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
              }}
              GooglePlacesSearchQuery={{
                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                rankby: 'distance',
                types: 'food',
              }}
              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
              predefinedPlaces={fixtures}
            />
          </View>
        </View>
        {/* <View style={styles.lowerContainer}>

        </View> */}
      </View>
    );
  }
}

export default Home;
