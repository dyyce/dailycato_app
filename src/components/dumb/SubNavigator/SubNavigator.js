import React, { Component, PropTypes } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  View,
  Text,
  TextInput
} from 'react-native'
import { Actions } from 'react-native-router-flux'
var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');

const styles = {
  container: {
    flex: 1,
    height: 42,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  leftIcon: {
    flex:1,
    height: 42,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rightIcon: {
    flex: 1,
    height: 42,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchBar: {
    flex: 3,
    height: 44,
    overflow: 'hidden'
  }
}

class SubNavigator extends Component {
  static propTypes = {
    filter: PropTypes.bool,
    close: PropTypes.bool,
    searchBar: PropTypes.bool
  }

  componentWillMount() {
    this.state = {
      text: 'search'
    }
  }

  renderLeftIcon() {
    let icon = this.props.close
      ? <Text>Close</Text>
      : <Text></Text>
      return icon
  }
  renderRightIcon() {
    let icon = this.props.filter
      ? <Text onPress={() => Actions.filter()}>Filter</Text>
      : <Text></Text>
      return icon
  }

  renderSearchBar() {
    let fixtures = [
      {description: 'Berlin', geometry: {location: {lat: 52.5200, lng: 13.4050} }},
      {description: 'Hamburg', geometry: {location: {lat: 53.5511, lng: 9.9937} }},
      {description: 'München', geometry: {location: {lat: 48.1351, lng: 11.5820} }},
      {description: 'Würzburg', geometry: {location: {lat: 49.7913, lng: 9.9534} }},
      // {description: 'Aschaffenburg', geometry: {location: {lat: 49.9807, lng: 9.1356} }}
    ];

    return <GooglePlacesAutocomplete
      style={styles.searchBar}
      placeholder='Search'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      fetchDetails={true}
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        console.log('data', data);
        console.log(details);
        // this.props.getRestaurantsAtLocation(data.geometry.location.lat, data.geometry.location.lng);
        // Actions.resultList({ title: data.description });
      }}
      getDefaultValue={() => {
        return ''; // text input default value
      }}
      enablePoweredByContainer={false}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyCcoU0umIB4G1i1BiSOqtyeoM2IqBuWE1g',
        language: 'de', // language of the results
        types: '(cities)', // default: 'geocode'
      }}
      styles={{
        description: {
          fontWeight: 'bold',
        },
        predefinedPlacesDescription: {
          color: '#1faadb',
        },
      }}

      currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food',
      }}
      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      predefinedPlaces={fixtures}
    />
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.leftIcon}>
          {this.renderLeftIcon()}
        </View>

        <View style={styles.searchBar}>
          {this.renderSearchBar()}
        </View>

        <View style={styles.rightIcon}>
          {this.renderRightIcon()}
        </View>
      </View>
    )
  }
}

export default SubNavigator
