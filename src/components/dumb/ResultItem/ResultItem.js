import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableHighlight,
  Image
} from 'react-native'

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#f3f3f3',
    marginBottom: 10,
  },
  touchContainer: {
    flex: 1,
    padding: 5
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch'
  },
  distanceText: {
    color: '#696969',
    marginRight: 20
  },
  separator: {
    marginLeft: -5,
    marginTop: 3,
    marginBottom: 3,
    width: 351,
    height: 1,
    backgroundColor: 'black'
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  }
}

class ResultItem extends Component {
  componentWillMount() {

  }

  render() {
    return <View style={styles.container}>
      <TouchableHighlight
        style={styles.touchContainer}
        onPress={this.props.onPress}
        underlayColor="white">
        <View>
          <View style={styles.headerContainer}>
            <Text>{this.props.item.name}</Text>
            <Text style={styles.distanceText}>distance</Text>
          </View>
          <View style={styles.separator}></View>
          <View style={styles.contentContainer}>
            <Image />
            <Text>{this.props.item.description}</Text>
          </View>
        </View>
      </TouchableHighlight>
    </View>
  }
}

export default ResultItem
