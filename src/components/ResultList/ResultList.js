import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  LayoutAnimation
} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import ResultItem from '../dumb/ResultItem'
import SubNavigator from '../dumb/SubNavigator'
import routes from '../../routes';

const styles = {
  container: {
    marginTop: 64,
    flex: 1
  }
}

class ResultList extends Component {
  _onPressListItem(item) {
    Actions.detailView({
      passProps: item,
      title: item.doc.name
    });
  }

  renderResults() {
    let list = this.props.locatedRestaurants.map((item, key) => {
      return <ResultItem
        onPress={this._onPressListItem.bind(this, item)}
        item={item.doc}
        key={key}>
      </ResultItem>;
    })
    return <View>{list}</View>;
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <SubNavigator filter={true}/>
        </View>

        <View>
          <ScrollView
            automaticallyAdjustContentInsets={false}
            scrollEventThrottle={200}
            style={styles.scrollView}>
            { this.props.locatedRestaurants ? this.renderResults() : null }
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ResultList;
