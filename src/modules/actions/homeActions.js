import request from 'axios';
// import request3 from 'superagent';
import * as types from '../constants/homeConstants';
import config from '../../../config'

let position = 0;
const serverUrl = `${config.serverUrl}:${config.port}`;
const getCityUrl = `http://maps.googleapis.com/maps/api/geocode/json?latlng=`;
const restaurantsLocationUrl = `${serverUrl}/api/1/restaurants`;

function getPlace(coords) {
  let url = `${getCityUrl}${coords.latitude},${coords.longitude}&sensor=true`;
  let request2 = axios.get(url, {
      // port: 443,
    }, (err, res, body) => {
      if(err) console.error(err);
      console.log('got place',res, body);
    }
  );
  return request2;
}

// modify that since user location is different here
export const locateUser = () => {
  return dispatch => {
    dispatch(locateUserRequest());
  }
}

export const locateUserRequest = () => {
  return {
    type: types.LOCATE_USER_REQUEST
  }
}

export const locateUserSuccess = (position) => {
  return {
    type: types.LOCATE_USER_SUCCESS,
    position,
    data: getPlace(position.coords)
  }
}

export const locateUserFailure = (error) => {
  return {
    type: types.LOCATE_USER_ERROR
  }
}

export function getRestaurantsAtLocation(lat, lng) {
  return dispatch => {
    dispatch(getRestaurantsAtLocationRequest());
    return request
    .get(`${restaurantsLocationUrl}/${lat}/${lng}`, {
      headers: {
        'Accept': 'application/json'
      },
      timeout: 1000
    })
    .then(res => {
      return dispatch(getRestaurantsAtLocationSuccess(res.data));
    })
    .catch(error => dispatch(getRestaurantsAtLocationFailure(error)))
  }
}

export function getRestaurantsAtLocationRequest() {
  return {
    type: types.GET_RESTAURANTS_LOCATION_REQUEST
  }
}

export function getRestaurantsAtLocationSuccess(locatedRestaurants) {
  return {
    type: types.GET_RESTAURANTS_LOCATION_SUCCESS,
    locatedRestaurants
  }
}

export function getRestaurantsAtLocationFailure(error) {
  return {
    type: types.GET_RESTAURANTS_LOCATION_FAILURE,
    error
  }
}
