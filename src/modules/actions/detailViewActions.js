import request from 'axios';
import * as types from '../constants/detailViewConstants';
import config from '../../../config'

const serverUrl = `${config.serverUrl}:${config.port}`;
const offersUrl = `${serverUrl}/api/0/getOffers`;

export const getOffers = (id) => {
  return dispatch => {
    dispatch(getOffersRequest(id))
    return request
    .get(`${offersUrl}/${id}`, {
      headers: {
        'Accept': 'application/json'
      }
    })
    .then(res => {
      return dispatch(getOffersSuccess(res.data));
    })
    .catch(error => dispatch(getOffersFailure(error)))
  }
}

export const getOffersRequest = (id) => {
  return {
    type: types.GET_OFFERS_BY_ID_REQUEST,
    restaurantId: id
  }
}

export const getOffersSuccess = (offers) => {
  return {
    type: types.GET_OFFERS_BY_ID_SUCCESS,
    offers
  }
}

export const getOffersFailure = (error) => {
  return {
    type: types.GET_OFFERS_BY_ID_FAILURE,
    error
  }
}
