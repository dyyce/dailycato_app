import ResultList from '../components/ResultList';
import Home from '../components/Home'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../modules/actions/resultListActions';

export default connect(
  state => ({
    locatedRestaurants: state.homeReducer.locatedRestaurants
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(ResultList);
