import Home from '../components/Home'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../modules/actions/homeActions';

export default connect(
  state => ({
    position: state.homeReducer.position,
    locatedRestaurants: state.homeReducer.locatedRestaurants
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(Home);
