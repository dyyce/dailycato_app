import Filter from '../components/Filter'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../modules/actions/filterActions';

export default connect(
  null,
  dispatch => bindActionCreators(actions, dispatch)
)(Filter);
