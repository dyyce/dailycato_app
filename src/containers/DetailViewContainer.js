import DetailView from '../components/DetailView'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../modules/actions/detailViewActions';


export default connect(
  state => ({
    // position: state.homeReducer.position,
    offers: state.detailReducer.offers
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(DetailView);
