import Home from './containers/HomeContainer';
import ResultList from './containers/ResultListContainer';
import DetailView from './containers/DetailViewContainer';
import Filter from './containers/FilterContainer';

const routes = {
  home: Home,
  resultList: ResultList,
  detailView: DetailView,
  filter: Filter
}

export default routes;
