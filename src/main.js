import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import { Scene, Router, Modal } from 'react-native-router-flux';
import thunk from 'redux-thunk'
import promise from 'redux-promise'
import createLogger from 'redux-logger'

import reducers from './reducers';
import routes from './routes';

const logger = createLogger();

let store = createStore(
  reducers,
  applyMiddleware(thunk, promise)
);
if(__DEV__) {
  console.log('dev mode active');
  store = createStore(
    reducers,
    applyMiddleware(logger, thunk, promise)
  )
}

const styles = {
  container: {
    flex: 1
  }
}

class MainApp extends Component {

  render() {
    return(
      <Provider store={store}>
        <Router>
          <Scene
            navigationBarStyle={{backgroundColor: '#F48B22'}}
            titleStyle={{color: 'white'}}
            key='root'>
            <Scene key='home' component={routes['home']} hideNavBar={true}></Scene>
            <Scene key='resultList' component={routes['resultList']} hideNavBar={false}></Scene>
            <Scene key='filter' direction='vertical'>
              <Scene key='filterResults' schema={'modal'} component={routes['filter']} title='Filter'></Scene>
            </Scene>
            <Scene key='detailView' component={routes['detailView']} title="DetailView">
            </Scene>
          </Scene>
        </Router>
      </Provider>
    )
  }
}

export default MainApp;
