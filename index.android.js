import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

import App from './src/main';

class dailycat_app extends Component {
  render() {
    return (
      <App></App>
    );
  }
}

AppRegistry.registerComponent('dailycat_app', () => dailycat_app);
